package com.example.springmongo.controllers;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springmongo.models.User;
import com.example.springmongo.repositories.UserRepository;

@RestController
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
//	@Autowired
//	private MongoTemplate mongoTemplate;
	
	@GetMapping("/")
	public String returnResponse() {
		return "Welcome to Spring Mongo";
	}
	
	@GetMapping("/users")
	public List<User> findAllUsers(){
		return userRepository.findAll();
	}
	
	@PostMapping("/users")
	public User createUser(@RequestBody User user){
		System.out.println(user);
//		mongoTemplate.find
		user.set_id(ObjectId.get());
		user.setCreated_at(new Date());
		user.setUpdated_at(new Date());
		User savedUser = userRepository.save(user);
		return savedUser;
	}
	
	@PutMapping("/users/{_id}")
	public User updateUser(@PathVariable ObjectId _id, @RequestBody User user){
		user.set_id(_id);
		user.getCreated_at();
		user.setUpdated_at(new Date());
		User updatedUser = userRepository.save(user);
		return updatedUser;
	}
	
	@GetMapping("/users/{_id}")
	public User getUserById(@PathVariable ObjectId _id) {
		return userRepository.findBy_id(_id);
		
	}
	
	@DeleteMapping("/users/{_id}")
	public void deleteUserById(@PathVariable ObjectId _id) {
		userRepository.delete(userRepository.findBy_id(_id));
	}
}
