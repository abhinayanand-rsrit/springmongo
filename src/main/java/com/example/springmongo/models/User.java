package com.example.springmongo.models;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document(collection = "user")
public class User {
	
	@Id
	public ObjectId _id;
	
	public String name;
	public String email;
	public String username;
	public String password;
	public String role;
	
	@Field
	public Number email_verify=0;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	public Date created_at;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	public Date updated_at;
	
	public User(ObjectId _id, String name, String email, String username, String password, String role,
			int email_verify, Date created_at, Date updated_at) {
		super();
		this._id = _id;
		this.name = name;
		this.email = email;
		this.username = username;
		this.password = password;
		this.role = role;
		this.email_verify = email_verify;
		this.created_at = created_at;
		this.updated_at = updated_at;
	}
	public String get_id() {
		return _id.toHexString();
	}
	public void set_id(ObjectId _id) {
		this._id = _id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Number getEmail_verify() {
		return email_verify;
	}
	public void setEmail_verify(int email_verify) {
		this.email_verify = email_verify;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	
	

}
