package com.example.springmongo.repositories;

import com.example.springmongo.models.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String>{
	public User findBy_id(ObjectId _id);
}
